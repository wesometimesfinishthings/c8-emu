#include "Chip8UI.hpp"
#include <vector>

Chip8::Chip8UI* currentChip8UI = 0;

Chip8::Chip8UI::Chip8UI(char* szTitle, char* pDisplay)
{
	windowName = szTitle;
	displayPtr = pDisplay;
	MakeWindow();
}

Chip8::Chip8UI::Chip8UI(char* pDisplay)
	: Chip8UI("Chip8 Emu", pDisplay)
{ }

Chip8::Chip8UI::~Chip8UI()
{
	currentChip8UI = 0;
	CloseWindow(hWnd);
	DeleteObject(backgroundBrush);
	DeleteObject(foregroundBrush);
}

void Chip8::Chip8UI::UpdateGraphics()
{
	InvalidateRect(hWnd, nullptr, true); // causes entire region to update.
	UpdateWindow(hWnd);
}

void Chip8::Chip8UI::SetForegroundColor(int r, int g, int b)
{
	HBRUSH oldBrush = foregroundBrush;
	foregroundBrush = CreateSolidBrush(RGB(r, g, b));
	DeleteObject(oldBrush);
}

void Chip8::Chip8UI::SetBackgroundColor(int r, int g, int b)
{
	HBRUSH oldBrush = backgroundBrush;
	backgroundBrush = CreateSolidBrush(RGB(r, g, b));
	DeleteObject(oldBrush);
}

void Chip8::Chip8UI::MakeWindow()
{
	// Important.
	if (currentChip8UI)
	{
		// Somehow we made at least two windows.
		// Two. fucking two.
		// How did this even happen?
		// Gotta let ourselves know just how bad it is.
		MessageBoxA(0, "There is already a window you fuckwit", "A window already exists", MB_OK);
		return;
	}

	currentChip8UI = this;
	hInst = GetModuleHandle(0);
	// Default black
	backgroundBrush = CreateSolidBrush(RGB(0, 0, 0));
	// Default white
	//foregroundBrush = CreateSolidBrush(RGB(255, 255, 255));
	// mambda white
	foregroundBrush = CreateSolidBrush( RGB( 123, 3, 188 ) );


	WNDCLASSEXA wcex;
	memset(&wcex, 0, sizeof(WNDCLASSEXA));
	wcex.cbSize = sizeof(WNDCLASSEXA);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = &Chip8UI::Chip8UIWndProc;
	wcex.hInstance = hInst;
	wcex.lpszClassName = "chip8windowclass";

	// If this breaks all hope is lost, as such there is no error check.
	RegisterClassExA(&wcex);

	hWnd = CreateWindowExA(WS_EX_OVERLAPPEDWINDOW, "chip8windowclass", windowName, WS_OVERLAPPEDWINDOW, // I think its kinda important, maybe.
		CW_USEDEFAULT, CW_USEDEFAULT, chip8w * 12, chip8h * 12, // Dimensions and shit
		0, 0, hInst, nullptr); // Garbage

	// We're super fucked. I broke it. Everything is falling apart.
	// Honestly the fact we got this far was good enough.
	if (!hWnd)
	{
		MessageBoxA(0, "Fucking shit", "Its broken.", MB_OK);
		return;
	}

	ShowWindow(hWnd, SW_SHOWDEFAULT );
	// update window doesnt matter here, we just need to update graphics whenever
}

LRESULT Chip8::Chip8UI::Chip8UIWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		RECT r;
		GetClientRect(hWnd, &r);
		// then you might have: 

		int screenWidth = r.right - r.left;
		int screenHeight = r.bottom - r.top;

		r.left = r.top = 0;
		r.right = screenWidth;
		r.bottom = screenHeight;
		FillRect(hdc, &r, currentChip8UI->backgroundBrush);

		int drawWidth = screenWidth / ( chip8w ); // actually hits the side of the screen
		int drawHeight = screenHeight / ( chip8h ); // cant go 1 lower otherwise the end if off screen whatever.

		// Draws in vertical strips which I don't think is accurate to 
		// the original C8 machines but whatever.
		char* display = currentChip8UI->displayPtr;
		for (int x = 0; x < 64; ++x)
		{
			for (int y = 0; y < 32; ++y)
			{
				if (display[x + (y * 64)])
				{
					int pX = x * drawWidth;
					int pY = y * drawHeight;
					r.left = pX;
					r.top = pY;
					r.right = pX + drawWidth;
					r.bottom = pY + drawHeight;

					if ( y == 31 || x == 63 )
						FillRect( hdc, &r, CreateSolidBrush( RGB( 255, 255, 255 ) ));
					else
						FillRect(hdc, &r, currentChip8UI->foregroundBrush);
				}
			}
		}

		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		ExitProcess( 0xDEADBABE );
		//PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return 0;
}



