#pragma once
#include "GenericHeader.hpp"

namespace Chip8
{
	class Chip8UI
	{
	public:
		Chip8UI(char* pDisplay);
		Chip8UI(char* szTitle, char* pDisplay);
		~Chip8UI();

		void UpdateGraphics();
		void SetForegroundColor(int r, int g, int b);
		void SetBackgroundColor(int r, int g, int b);

	private:
		void MakeWindow();
		static LRESULT CALLBACK Chip8UIWndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);

		char* displayPtr = 0;
		HBRUSH backgroundBrush = 0;
		HBRUSH foregroundBrush = 0;
		HWND hWnd = 0;
		HINSTANCE hInst = 0;
		char* windowName = 0;

		static constexpr int chip8w = 64;
		static constexpr int chip8h = 32;

	};
}
