#include "Chip8.hpp"
#include <chrono>
#include <thread>

Chip8::Chip8::Chip8()
{
	srand( time( 0 ) );

	char fontset[] = { 
		0xF0, 0x90, 0x90, 0x90, 0xF0,	// 0
		0x20, 0x60, 0x20, 0x20, 0x70,	// 1
		0xF0, 0x10, 0xF0, 0x80, 0xF0,	// 2
		0xF0, 0x10, 0xF0, 0x10, 0xF0,	// 3
		0x90, 0x90, 0xF0, 0x10, 0x10,	// 4
		0xF0, 0x80, 0xF0, 0x10, 0xF0,	// 5
		0xf0, 0x80, 0xf0, 0x90, 0xf0,	// 6
		0xf0, 0x10, 0x20, 0x40, 0x40,	// 7
		0xf0, 0x90, 0xf0, 0x90, 0xf0,	// 8
		0xf0, 0x90, 0xf0, 0x10, 0xf0,	// 9
		0xf0, 0x90, 0xf0, 0x90, 0x90,	// A
		0xE0, 0x90, 0xE0, 0x90, 0xE0,	// B
		0xf0, 0x80, 0x80, 0x80, 0xf0,	// C
		0xe0, 0x90, 0x90, 0x90, 0xe0,	// D
		0xf0, 0x80, 0xf0, 0x80, 0xf0,	// E
		0xf0, 0x80, 0xf0, 0x80, 0x80,	// F
	};

	memset(&this->registers, 0, sizeof(this->registers)); //Reset Registers
	memset(&this->display, 0, sizeof(this->display)); //Reset Display
	memset(&this->memory, 0, sizeof(this->memory)); // Reset Memory
	memcpy(fontset, &this->display, sizeof(fontset) );
	this->registers.ip = Chip8::rom_start;

	/*
	1[q] 2[w] 3[e] C[r]
	4[a] 5[S] 6[d] D[f]
	7[z] 8[s] 9[c] E[v]
	A[1] 0[2] B[3] F[4]
	*/
	this->KeyMap[0x1] = 0x51; // Q
	this->KeyMap[0x2] = 0x57; // W
	this->KeyMap[0x3] = 0x45; // E
	this->KeyMap[0xC] = 0x52; // R

	this->KeyMap[0x4] = 0x41; // A
	this->KeyMap[0x5] = VK_SPACE;
	this->KeyMap[0x6] = 0x44; // D
	this->KeyMap[0xD] = 0x46; // F

	this->KeyMap[0x7] = 0x5A; // Z
	this->KeyMap[0x8] = 0x53; // S
	this->KeyMap[0x9] = 0x43; // C
	this->KeyMap[0xE] = 0x56; // V

	this->KeyMap[0xA] = 0x31; // 1
	this->KeyMap[0x0] = 0x32; // 2
	this->KeyMap[0xB] = 0x33; // 3
	this->KeyMap[0xF] = 0x34; // 4
	Log("Reset");
}
uint16_t Chip8::Chip8::GetInstruction()
{
	struct Eazy {
		union {
			uint16_t opcode;
			struct {
				uint8_t back;
				uint8_t front;
			};
		};
	};
	
	if ( this->skipNext )
	{
		this->registers.ip += 2;
		this->skipNext = false;
	}

	auto instr = *( uint16_t* )&this->memory[ this->registers.ip ];
	Eazy e /*R.I.P.*/ = {};
	e.front = instr & 0xFF;
	e.back = ( instr & 0xFF00 ) >> 8;

	this->registers.ip += 2; // increment ip plez.
	return e.opcode;
}

int yPos = 0;
int xPos = 0;

int ticks = 0;
std::clock_t timer = std::clock();

void Chip8::Chip8::tick( )
{
	/*Need a way to time */
	auto timeVal = ( float )std::clock( ) - ( float )this->timerClock;
	if ( timeVal >= this->timerspeed / 10.f )
	{
		if ( this->timerTicks < this->timerspeed )
		{
			this->timerTicks++;
			if ( this->registers.delay )
			{
				if ( --this->registers.delay == 0 )
				{/*whatever*/ }
			}

			if ( this->registers.sound )
			{
				if ( --this->registers.sound != 0 )
				{
					/*sound*/
					if ( !this->playingSound )
					{
						//MessageBeep( MB_ICONQUESTION );
					}
				}
			}
		}

		this->timerClock = std::clock( );
	}

	auto val = ( float )( std::clock( ) - this->cpuClock );
	if ( val >= this->clockspeed / ( float )CLOCKS_PER_SEC )
	{
		ticks++;
		if ( this->romLoaded )
		{
			auto instruction = this->GetInstruction( );
			//Log( "%02X", instruction );

			auto end = std::end( Instructions::instruction_set );
			auto found = std::find( std::begin( Instructions::instruction_set ), end, instruction );
			//Execute OPCODE
			if ( found != end )
				found->effect( this, found->opcode, instruction );
			else
			{
				Log( "Unknown instruction %02X", instruction );
				getchar( );
			}

			if ( this->drawFlag )
				this->UpdateScreen( );
		}

		this->cpuClock = std::clock( );
	}

	auto scnd = ( ( std::clock( ) - timer ) / ( float )CLOCKS_PER_SEC );
	if ( scnd >= 1.f )
	{
		Log( "We have %d clock ticks in %.2f seconds", this->timerTicks, scnd );
		this->timerTicks = 0;
		Log( "We have %d cpu ticks in the same time.", ticks );
		ticks = 0;
		timer = std::clock( );
	}


	// sleep either until next clock cycle or timer cycle.
	auto ctime = ( std::clock( ) - this->timerClock );
	std::this_thread::sleep_for( std::chrono::microseconds( ( long )( this->clockspeed - ctime ) ) );
	//SleepEx( ctime, true );
	//Log( "We have %.2f seconds until next tick.", ( this->tickrate / CLOCKS_PER_SEC ) - time );
}

char * Chip8::Chip8::GetMemory() 
{
	return this->memory;
}

char * Chip8::Chip8::GetDisplay()
{
	return this->display;
}

bool Chip8::Chip8::LoadRom(const char * rom)
{
	std::ifstream fROM;
	fROM.open(rom, std::ifstream::binary | std::ios::ate);

	if (!fROM)
	{
		Log("Couldn't open the rom. Are you sure the name is right? (Don't forget the extension)");
		return false;
	}

	int len = fROM.tellg();
	fROM.seekg(0);
	if ( !fROM.read( &this->memory[Chip8::rom_start], len) )
	{ 
		// writing to adderss 0 luul.
		Log("Not able to read the rom");
		return false; 
	}

	Log("Shit read successfully");
	this->romLoaded = true;
	return true;
}

void Chip8::Chip8::SetSkip( bool skip )
{
	this->skipNext = skip;
}

void Chip8::Chip8::SetTick( float tickrate )
{
	this->clockspeed = tickrate;
}

bool Chip8::Chip8::IsRomLoaded( )
{
	return this->romLoaded;
}

bool Chip8::Chip8::SetPixel(int x, int y, int height)
{
	if ( x > 63 || y > 31 )
		return false;

	// loop through height and set pixels.
	auto mem = this->GetMemory();
	auto display = this->GetDisplay();
	this->registers.VF = 0; // start as 0.
	for (auto curHeight = 0; curHeight < height; curHeight++) //Height
	{
		auto sprite = mem[this->registers.spritesheet + curHeight];
		for (auto curWidth = 0; curWidth < 8; curWidth++) // every draw operation is 8 wide.
		{
			auto shouldDraw = ( sprite & ( 0x80 >> curWidth ) ) != 0;
			if ( shouldDraw )
			{
				if ( display[ curWidth + x + ( ( y + curHeight ) * 64 ) ] == 1 )
					this->registers.VF = 1; //It was flipped [Collision occured]

				display[ ( curWidth + x ) + ( ( y + curHeight ) * 64 ) ] ^= 1; //nice sht TWOVEEN
				this->drawFlag = true; // lets draw now then.
			}
		}
	}
	/* If the sprite is positioned so part of it is outside the
	coordinates of the display, it wraps around to the opposite side of the screen. */
	//Dunno if that's necessary to implement but just give me a heads up if you want it
	return this->registers.VF;
}

void Chip8::Chip8::UpdateScreen()
{
	if (currentUI)
		currentUI->UpdateGraphics();

	this->drawFlag = false;
}

void Chip8::Chip8::SetCurrentUI(Chip8UI * ui)
{
	currentUI = ui;
}

