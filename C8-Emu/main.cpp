#include "Chip8.hpp"
#include "Chip8UI.hpp"

int main( int argc, char ** argv )
{
	/* Load rom from cmd losers */
	char * rom = argv[ 1 ];

	if ( argc < 2 ) {
		Log( "To load a custom game, follow this usage\nC8-Emu <full_path_to_rom>" ); 
		Log( "We will now load a standard rom." );

		rom = R"(c8games\BRIX)";
	}

	Chip8::Chip8 chip8 = Chip8::Chip8( );
	if ( !chip8.LoadRom( rom ) ) { return 0; }
	Chip8::Chip8UI emuUI = Chip8::Chip8UI( "test", chip8.GetDisplay( ) );
	chip8.SetCurrentUI( &emuUI );
	MSG msg;

	while( true )
	{
		chip8.tick( ); // will sleep as necessary.

		if ( PeekMessage( &msg, nullptr, 0, 0, 0 ) )
		{
			GetMessage( &msg, nullptr, 0, 0 );
			TranslateMessage( &msg );
			DispatchMessage( &msg );
		}
	}
}