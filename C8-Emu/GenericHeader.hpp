#pragma once
#include <Windows.h>
#include <iostream>
#include <fstream>

#define DBG 1

#if DBG
#define Log(format, ...)	printf_s("[ %-20s ] ", __FUNCTION__); \
						printf_s( format, __VA_ARGS__); \
						printf_s("\n");
#else
#define Log //
#endif